package gamf.navigationapp;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Profile currentProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar mainToolbar = findViewById(R.id.MainToolbar);
        setSupportActionBar(mainToolbar);

        loadFragment(new HomeFragment(), "home", false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    private void loadFragmentAndAddToBackStack(Fragment fragment, String tag) {
        loadFragment(fragment, tag, true);
    }

    private void loadFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.ContainerFrameLayout, fragment, tag);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                loadFragmentAndAddToBackStack(HomeFragment.newInstance(), "home");
                return true;

            case R.id.action_profile:
                loadFragmentAndAddToBackStack(ProfileFragment.newInstance(), "profile");
                return true;

            case R.id.action_favourite:
                loadFragmentAndAddToBackStack(FavouritesFragment.newInstance(), "favourites");
                return true;

            case R.id.action_settings:
                Intent settingsIntent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(settingsIntent);
                return true;

            default:
                showMessage("Default value");
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveProfile(Profile profile) {
        currentProfile = profile;
    }

    public Profile getCurrentProfile() {
        return currentProfile;
    }

    public void navigateToHome() {
        loadFragmentAndAddToBackStack(HomeFragment.newInstance(), "home");
    }

    private void showMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
