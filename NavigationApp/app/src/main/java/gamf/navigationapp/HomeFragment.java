package gamf.navigationapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class HomeFragment extends Fragment {

    private ImageView profileImageView;
    private TextView profileNameTextText;

    public HomeFragment() {
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        profileImageView = rootView.findViewById(R.id.ProfileImageView);
        profileNameTextText = rootView.findViewById(R.id.ProfileNameTextView);

        Profile profile = ((MainActivity)getActivity()).getCurrentProfile();
        if(profile != null ) {
            if(profile.getImage() != null) {
                profileImageView.setImageBitmap(profile.getImage());
            } else {
                profileImageView.setBackgroundResource(R.color.colorAccent);
            }

            profileNameTextText.setText(profile.getName() == null || profile.getName().isEmpty()
                    ? "Profil beállítás alatt" : profile.getName());
        } else {
            profileNameTextText.setText("Profil beállítás alatt");
        }

        return rootView;
    }
}
