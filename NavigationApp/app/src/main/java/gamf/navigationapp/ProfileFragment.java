package gamf.navigationapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import static android.app.Activity.RESULT_OK;


public class ProfileFragment extends Fragment {

    private static final int CAMERA_REQUEST_CODE = 1;

    private ImageView profileImageView;
    private EditText profileNameEditText;
    private Button saveButton;

    private Bitmap profileImage;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        profileImageView = rootView.findViewById(R.id.ProfileImageView);
        profileNameEditText = rootView.findViewById(R.id.ProfileNameEditText);
        saveButton = rootView.findViewById(R.id.SaveButton);

        Profile currentProfile = ((MainActivity)getActivity()).getCurrentProfile();
        if(currentProfile != null) {
            profileImageView.setImageBitmap(currentProfile.getImage());
            profileNameEditText.setText(currentProfile.getName());
        }

        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Profile profile = new Profile(profileImage, profileNameEditText.getText().toString());

                MainActivity mainActivity = (MainActivity)getActivity();
                mainActivity.saveProfile(profile);
                mainActivity.navigateToHome();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            profileImage = (Bitmap) data.getExtras().get("data");
            profileImageView.setImageBitmap(profileImage);
        }
    }
}
